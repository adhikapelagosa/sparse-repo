$(function () {
    $('#chart-1').highcharts({
        chart: {
            type: 'areaspline'
        },
        title: {
            text: 'Chart Title'
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'top',
            x: 150,
            y: 100,
            floating: true,
            borderWidth: 1,
            backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'
        },
        xAxis: {
            categories: [
                'Monday',
                'Tuesday',
                'Wednesday',
                'Thursday',
                'Friday',
                'Saturday',
                'Sunday'
            ]
        },
        yAxis: {
            title: {
                text: 'Y Axis Title'
            }
        },
        tooltip: {
            shared: true,
            valueSuffix: ' units'
        },
        credits: {
            enabled: false
        },
        plotOptions: {
            areaspline: {
                fillOpacity: 0.5
            }
        },
        series: [{
            name: 'Lorem ipsum 1',
            color: 'rgba(170, 102, 193, 0.5)',
            data: [3, 4, 3, 5, 4, 10, 12]
        }, {
            name: 'Lorem ipsum 2',
            color: 'rgba(248, 231, 28, 0.5)',
            data: [1, 3, 4, 3, 3, 5, 4]
        }]
    });
});

$(function () {

    $(document).ready(function () {

        // Build the chart
        $('#chart-2').highcharts({
            chart: {
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false,
                type: 'pie'
            },
            title: false,
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
            },
            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: false
                    },
                    showInLegend: true
                }
            },
            series: [{
                name: 'Percentage',
                colorByPoint: true,
                data: [{
                    name: 'Rebound',
                    color: 'rgba(248, 231, 28, 0.5)',
                    y: 75
                }, {
                    name: 'Navigate',
                    color: 'rgba(170, 102, 193, 0.5)',
                    y: 25
                }]
            }]
        });
    });
});

$(function () {
    $('#chart-3').highcharts({
        chart: {
            type: 'column'
        },
        title: {
            text: false
        },
        subtitle: {
            text: false
        },
        xAxis: {
            categories: [
                '22/09/2016',
                '23/09/2016',
                '24/09/2016'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Text vertical'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Value 1',
            color: 'rgb(255, 188, 111)',
            data: [49.9, 71.5, 106.4]

        }, {
            name: 'Value 2',
            color: 'rgb(200, 245, 246)',
            data: [83.6, 78.8, 98.5]

        }]
    });
});

$('input:not(.ichecklight)').iCheck({
  radioClass: 'iradio_square-aero',
  increaseArea: '20%' // optional
});

$(document).ready(function() {
    $('table.datatables-overview-small').DataTable( {
        "pageLength": 5,  
        "bSort": false,
        "info": false,
        "searching": false,
        "bLengthChange": false,      
        "pagingType": "simple",
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "dom": '<"pagination-tab"tp>'
    } );
} );
$(document).ready(function() {
    $('table.datatables-overview-big').DataTable( {
        "pageLength": 34,  
        "bSort": false,
        "info": false,
        "searching": false,
        "bLengthChange": false,      
        "pagingType": "simple",
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        },
        "dom": '<"pagination-tab"tp>'
    } );
} );
$(document).ready(function() {
    $('table.datatables-test').DataTable( {
        "pageLength": 5,  
        "info": false,
        "searching": false,
        "bLengthChange": false,      
        "pagingType": "simple",
        "language": {
            "paginate": {
                "previous": "<",
                "next": ">"
            }
        }
    } );
} );
$(document).ready(function() {
    $('table.datatables-test-2').DataTable( {
        "columnDefs": [ {
            "targets": 'no-sort',
            "orderable": false
        } ],
        "pageLength": 5,  
        "info": false,
        "searching": false,     
        "dom": '<"top"i>rt<"bottom"flp><"clear">',
    } );
} );

$('#password').password();

